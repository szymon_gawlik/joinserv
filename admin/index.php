<?php
require_once '../Class/MyAutoloader.php';
include_once '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
$session = new Session();
$request = Request::createFromGlobals();

if(empty($session->get('LoginId')) || $session->get('Permission') === 0) {
    $redirect = new RedirectResponse('../index.php');
    $redirect->send();
    exit;
}
?>
<!DOCTYPE html>
<html lang="pl" ng-app="AdminApp">
<head>
    <META http-equiv=Content-Type content="text/html; charset=UTF-8">
    <title>JoinServ.eu - Panel Admina</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../web/js/jquery.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,200,300,600,700' rel='stylesheet' type='text/css'>
    <script src="../web/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../web/js/angular.min.js"></script>
    <script type="text/javascript" src="../web/js/angular-route.min.js"></script>
    <script src="../web/js/ngDialog.min.js"></script>
    <link rel="stylesheet" href="../web/style/ngDialog.css">
    <link rel="stylesheet" href="../web/style/ngDialog-theme-default.min.css">
    <link rel="stylesheet" href="../web/style/ngDialog-theme-plain.min.css">
    <link rel="stylesheet" href="../web/style/bootstrap.min.css">
    <link data-require="bootstrap-css@2.3.2" data-semver="2.3.2" rel="stylesheet" href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" />

    <script data-require="angular-ui-bootstrap@0.3.0" data-semver="0.3.0" src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.3.0.min.js"></script>
    <script src="../web/js/AdminApp.js" type="text/javascript"></script>
</head>
<body>
<div class="container">
    <div class="row" ng-controller="MainCtrl">
        <div class="col-md-2">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a style="text-align: center"><strong>Newsy</strong></a></li>
                    <li><a href="?t=news&a=add">Dodaj</a></li>
                    <li><a href="?t=news">Edytuj</a></li>
                <?php if($session->get('Permission') == 2):?>
                <li class="active"><a style="text-align: center"><strong>Serwery</strong></a></li>
                    <li><a href="?t=servers">Wyświetl serwery</a></li>
                    <li><a href="?t=servers&a=networks">Sieci serwerów</a></li>

                <li class="active"><a style="text-align: center"><strong>Serwery promowane</strong></a></li>
                    <li><a href="?t=promoserver">Wyświetl serwery promowane</a></li>

                <li class="active"><a style="text-align: center"><strong>Użytkownicy</strong></a></li>
                    <li><a href="?t=users">Użytkownicy</a></li>
                    <li><a href="?t=review">Opinie</a></li>
                    <li><a href="?t=users&a=mailing">Wyślij maila do użytkowników</a></li>
                <li class="active"><a style="text-align: center"><strong>Mapy</strong></a></li>
                    <li><a href="?t=maps">Akceptuj miniatury map</a></li>

                <li class="active"><a style="text-align: center"><strong>Strony</strong></a></li>
                    <li><a href="?t=sites">Edytuj strony</a></li>

                <li class="active"><a style="text-align: center"><strong>Cennik</strong></a></li>
                    <li><a href="?t=prices">Zmień cennik</a></li>

                <li class="active"><a style="text-align: center"><strong>Płatności</strong></a></li>
                    <li><a href="?t=homepay">Ustawienia HomePay</a></li>
                    <li><a href="?t=homepay&a=sms">Ustawienia płatności sms</a></li>
                <?php endif; ?>
            </ul>
        </div>
        <div class="col-md-10">
            <?php
                $titles = ['news','servers','users','sites','homepay','prices','maps','promoserver','review'];
                $actions = ['add','networks','mailing','sms'];

                $title = $request->get('t');
                $action = $request->get('a');

            if(in_array($title,$titles)) {
                if(isset($action) && in_array($action,$actions)){
                    include_once '../web/template/admin/'.$title.'/'.$action.'.html';
                } else {
                    include_once '../web/template/admin/'.$title.'/'.$title.'.html';
                }
            }
            ?>
        </div>
    </div>
</div>
</body>
</html>