<?php
require_once 'Class/MyAutoloader.php';
include_once 'vendor/autoload.php';
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

$session = new Session();
$request = Request::createFromGlobals();
$session->start();
//$session->clear();
?>

<!DOCTYPE html>
<html lang="pl" ng-app="App">
<head>
	<META http-equiv=Content-Type content="text/html; charset=UTF-8">
	<title>[TYTUŁ STRONY]</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="web/styles/main.css">
	<link rel="stylesheet" type="text/css" href="web/styles/style.css">

	<script type="text/javascript" src="web/js/angular.min.js"></script>
	<script type="text/javascript" src="web/js/angular-route.min.js"></script>
	<link data-require="bootstrap-css@2.3.2" data-semver="2.3.2" rel="stylesheet" href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" />

	<script src="web/js/jquery.min.js"></script>
	<script type="text/javascript" src="web/js/toggler.js"></script>
	<script type="text/javascript" src="web/js/responsive.js"></script>
	<script src="web/js/bootstrap.min.js"></script>
	<script src="web/js/ngDialog.min.js"></script>
	<script data-require="angular-ui-bootstrap@0.3.0" data-semver="0.3.0" src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.3.0.min.js"></script>

	<link rel="stylesheet" href="web/style/ngDialog.css">
	<link rel="stylesheet" href="web/style/ngDialog-theme-default.min.css">
	<link rel="stylesheet" href="web/style/ngDialog-theme-plain.min.css">
	<script src="web/js/MyApp.js" type="text/javascript"></script>
	<link href='https://fonts.googleapis.com/css?family=Raleway:200,700' rel='stylesheet' type='text/css'>
</head>
<body ng-controller="MainCtrl">
	<div id="showsidebar" onclick="toggleSidebar()"><img src="web/images/ShowSidebar.png" /></div>
<header>
	<div id="headerstuff" class="col-10 col-xs-11 col-s-12 centered">
		<div id="logo" class="text-center left">
			<a href=""><img src="web/images/logo.png" /></a>
		</div>
		<nav id="primary_nav" class="col-9 col-xs-9 col-s-9 pull-right">
		<ul>
			<li><a href="index.php">Strona główna</a></li>
			<li><a href="?t=news">Newsy</a></li>
			<li><a href="?t=servers">Lista serwerów</a></li>
			<li><a href="?t=site&site=cennik">Cennik</a></li>
			<li>Serwis
				<ul>
					<li><a href="?t=site&site=kontakt">Kontakt</a></li>
					<li><a href="?t=site&site=regulamin">Regulamin</a></li>
					<li><a href="?t=site&site=faq">FAQ</a></li>
					<li><a href="?t=opinie">Opinie</a></li>
				</ul>
			</li>
		</ul>
		</nav>
	</div> <!-- Menu -->
	<div id="welcome">
		<div class="logo">Dodaj swój serwer <strong>za darmo</strong></div>
		<div class="logo2">Dołącz do naszej społeczności</div><?php if(!$session->get('LoginId')): ?>
		<div><a class="button button-1" href="?t=register">Rejestracja</a></div>
		<?php endif; ?>
	</div><!-- Powitanie -->
</header>
<div id="wrapper">
	<div id="main" class="col-9 col-xs-11 col-s-12 centered">
		<?php

		$sites = array('register','edytujProfil','reklamuj','doladujPortfel','profile_servers','servers','serverinfo','site','pobierz','news','dodajSiec','mojeSieci','dodajOpinie','opinie');
		$sitePHP = array('doladujPortfel','serverinfo','site','pobierz','banery','kontakt','regulamin','faq');
		$title = input($request->get('t'));

		if(empty($title)):
		?>
			<div id="motd">
				<div class="motd-content">
					<span class="title">Otwarcie serwisu</span><br>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac neque in risus porta placerat sit amet quis mi. Fusce vulputate felis eu imperdiet aliquam. In mattis interdum orci eget pellentesque. Phasellus viverra est lacus, eu luctus lectus aliquet sit amet. Aliquam luctus tincidunt ultricies. Aenean urna augue, venenatis ac pellentesque eu, auctor nec tellus. Donec dapibus quis turpis vitae maximus. Duis nec arcu in turpis consectetur tristique. Quisque commodo ipsum sit amet enim dignissim pellentesque. Nulla eget tempor quam, quis dapibus nisl. Vestibulum ac consectetur nulla. Donec porttitor imperdiet sagittis. Fusce quis dignissim nulla, sed efficitur justo.</p><hr>
					<span style="color:#9F9F9F">11.08.2015</span><a href=""><span class="right">Czytaj więcej</span></a>
				</div>
				<div class="motd-img">
					<img src="web/images/motd.png">
				</div>
			</div><!-- Najnowszy news -->
		<?php endif; ?>
		<div id="site">
			<div id="content">
				<?php
				if(in_array($title,$sites)) {
					(in_array($title,$sitePHP)) ? include_once 'web/template/'.$title.'.php' : include_once 'web/template/'.$title.'.html';

				} else {
				?>
				<section class="servers-view">
					<h1 class="special-header"><img src="web/images/star.png">Lista promowanych serwerów</h1>
					<table ng-controller="GetServers" ng-init="GetServers('promo')">
						<tr ng-repeat="server in servers | orderBy: 'position'">
							<td><img src="web/images/gry/csgo.png"></td>
							<td><a href="?t=serverinfo&ip={{server.ip}}">{{ server.info.HostName }}</a></td>
							<td>{{ server.ip }}</td>
							<td><div class="progressbar"><div class="progressbar-progress" ng-style="ProgressBar(server.info.Players,server.info.MaxPlayers)"></div><div class="progressbar-count">{{ server.info.Players }}/{{ server.info.MaxPlayers }}</div></div></td>
							<td>{{ server.info.Map }}</td>
							<td><a target="_blank" href="https://www.gametracker.com/server_info/{{server.ip}}"><img src="web/images/gametracker.png"></a><a href="steam://connect/{{server.ip}}"> <img src="web/images/steam.png"></a></td>
						</tr>
					</table>
				</section> <!-- Promowane serwery -->
				<section>
					<h1><img src="web/images/info.png">Aktualności</h1>
					<div class="news" ng-repeat="new in News | limitTo: 5">
						<h3>{{new.subject}}<span class="right">{{new.date}}</span></h3>
						<p ng-bind-html="new.content | trusted"></p>
					</div>
				</section> <!-- Aktualności -->

					<section id="circles" ng-init="GetCircleInfo()">
						<div>
							<div class="circle2"></div>
							<div class="circle">
								<h2>{{ ourServers }}</h2>
								serwerów na liście
							</div>
						</div>
						<div>
							<div class="circle2"></div>
							<div class="circle">
								<h2>{{ ourUsers }}</h2>
								łącznie userów
							</div>
						</div>
						<div>
							<div class="circle2"></div>
							<div class="circle">
								<h2>{{ ourNetworks }}</h2>
								sieci serwerów
							</div>
						</div>
					</section>
				<?php } ?>
			</div>
			<div id="sidebar">
				<?php if(!$session->get('LoginId')): ?>
					<form id="Login">
						<h1>Zaloguj się!</h1>
						<div id="alertLogin"></div>
						<div class="form-group">
							<label for="login">Login:</label>
							<input type="text" id="login" name="login" required>
						</div><br>
						<div class="form-group">
							<label for="login_pw">Hasło:</label>
							<input type="password" id="login_pw" name="pw" required>
						</div>

						<button class="btn btn-success" ng-click="SendForm('Login')">Zaloguj!</button> lub <a href="?t=register">Zarejestruj się</a>
					</form> <!-- Formularz logowania -->
				<?php else: ?>
				<section>
					<h1><img src="web/images/profil.png">Profil<span class="right pa-link" ng-if="login.admin > 0"><a href="admin">Panel Administratora</a></span></h1>
					<div class="sbar-profile">
						<div class="sbar-profile-avatar left text-right"><img height="50" width="50" src="web/uploaded/avatars/{{login.avatar}}"></div>
						<div class="sbar-profile-info left">
							<h3 style="font-size: 18px;">Witaj, <strong>{{ login.login }}</strong></h3>
							<span style="color:#9F9F9F;">Stan portfela: <strong>{{ login.wallet }}</strong> zł</span>
						</div>
						<div class="clear"></div>
					</div>
					<div class="profile-nav">
						<ul>
							<li><img src="web/images/profile/konto.png"><a href="?t=edytujProfil">Moje konto</a></li>
							<li><img src="web/images/profile/serwery.png"><a href="?t=profile_servers#content">Serwery</a></li>
							<li class="toggle" toggle="menucat3"><img src="web/images/profile/sieci.png">Sieci</li>
							<ul id="menucat3">
								<li><a href="?t=dodajSiec">Dodaj sieć</a></li>
								<li><a href="?t=mojeSieci">Moje sieci</a></li>
							</ul>
							<li><img src="web/images/profile/portfel.png"><a href="?t=doladujPortfel">Portfel</a></li>
							<li><img src="web/images/profile/opinia.png"><a href="?t=dodajOpinie">Dodaj opinię</a></li>
							<li><img src="web/images/profile/reklamuj.png"><a href="?t=reklamuj">Reklamuj się</a></li>
						</ul>
					</div>
				</section> <!-- Profil -->
				<?php endif; ?>
				<section>
					<h1><img src="web/images/partnerzy.png" style="margin-top: 1px;">Partnerzy</h1>
					<div class="partner"><img src="web/images/partnerzy/1.png"></div>
					<div class="partner"><img src="web/images/partnerzy/2.png"></div>
					<div class="partner"><img src="web/images/partnerzy/3.png"></div>
				</section> <!-- Partnerzy -->
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<footer>
	<div id="footerstuff" class="col-10 col-xs-11 col-s-12 centered text-center">
		<div class="left">
			&copy; joinserv.eu<br>
			Wszystkie prawa zastrzeżone<br>
		</div>
		<div class="right text-center"><br>
			<a href="#headerstuff"><img src="web/images/up.png" alt="powrót na górę"></a>
		</div>
		<div class="clear"></div>
	</div>
</footer> <!-- Stopka -->
</body>
</html>