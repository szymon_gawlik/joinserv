--
-- MySQL 5.5.46
-- Mon, 18 Jan 2016 12:26:23 +0000
--

CREATE TABLE `homepay_settings` (
   `id` int(11) not null auto_increment,
   `id_user` int(11),
   `acc_id` int(11),
   `acc_hash` varchar(50),
   `public_key` varchar(100),
   `private_key` varchar(100),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `map_image` (
   `id` int(11) not null auto_increment,
   `name` varchar(50),
   `accept` int(1) default '0',
   `extension` varchar(5),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `news` (
   `id` int(11) not null auto_increment,
   `subject` varchar(255),
   `content` text,
   `date` varchar(25),
   `edit` varchar(50),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `orders` (
   `id` int(11) not null auto_increment,
   `amount` int(11),
   `user_id` int(11),
   `date` varchar(50),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `pricing` (
   `id` int(11) not null auto_increment,
   `service` varchar(200),
   `price` int(11),
   `name` varchar(200),
   `expire` int(11),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `reviews` (
   `id` int(11) not null auto_increment,
   `name` varchar(200),
   `content` text,
   `date` varchar(50),
   `accept` int(11) default '0',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE `servers` (
   `id` int(11) not null auto_increment,
   `position` int(11) default '0',
   `ip_adress` varchar(25),
   `ip_port` int(5),
   `type` int(2),
   `promo` int(1) default '0',
   `promo_expire` varchar(50),
   `promo_amount` int(11),
   `owner` int(20),
   `highlight` varchar(20),
   `highlight_expire` varchar(50),
   `accept` int(1) default '0',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `servers_network` (
   `id` int(11) not null auto_increment,
   `name` varchar(255),
   `date` varchar(50),
   `server_count` int(20),
   `website` varchar(255),
   `owner` int(20) not null,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `sites` (
   `id` int(11) not null auto_increment,
   `subject` varchar(255),
   `content` text,
   `site` varchar(255),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `sms_api` (
   `id` int(11) not null auto_increment,
   `sms_id` varchar(20),
   `number` int(10),
   `code` varchar(20),
   `amount` varchar(6),
   `price` varchar(10),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `temp_order` (
   `id` int(11) not null auto_increment,
   `amount` int(11),
   `user_id` int(11),
   `date` varchar(50),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE `accounts` (
   `id` int(11) not null auto_increment,
   `login` varchar(200),
   `password` varchar(255),
   `email` varchar(255),
   `date` varchar(25),
   `admin` int(1) default '0',
   `wallet` int(11) default '0',
   `avatar` varchar(50) default 'avatar.png',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=2;

INSERT INTO `accounts` (`id`, `login`, `password`, `email`, `date`, `admin`, `wallet`, `avatar`) VALUES
('1', 'Admin', '$2y$10$A2ZD1WirmprQP4mhp4W6huXvrmB4DaynSTuHDJ34uELqF5axqzvAS', 'admin@joinserv.eu', '18.01.2016 13:24:04', '2', '0', 'avatar.png');

CREATE TABLE `server_type` (
   `id` int(11) not null auto_increment,
   `name` varchar(50),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=20;

INSERT INTO `server_type` (`id`, `name`) VALUES
('1', '4Fun'),
('2', 'Aim'),
('3', 'Battlefield Mod'),
('4', 'Zombie Mod'),
('5', 'Team Play'),
('6', 'Surf'),
('7', 'Warcraft Mod'),
('8', 'Real Mod'),
('9', 'Predator Mod'),
('10', 'PokeMod'),
('11', 'PaintBall Mod'),
('12', 'Hide and Seek'),
('14', 'DiabloMod'),
('15', 'DeathRun'),
('16', 'DeathMatch'),
('17', 'Call of Duty Mod'),
('18', 'Free For All'),
('19', 'Inne');