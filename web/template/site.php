<section ng-init="GetSite('<?php echo $_GET['site'] ?>')">
    <div id="motd">
        <div class="motd-content">
            <h1>{{ Site.subject }}</h1>
            <p ng-bind-html="Site.content | trusted">
            </p>
        </div>
    </div>
</section>