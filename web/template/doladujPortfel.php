<section ng-init="CheckLogin()">
    <div id="motd">
        <h1>Wybierz kwote doładowania</h1><br>
        <?php
        if($_GET['error'] == 'psc') {
            echo '<h2>Wystąpił błąd płatności PSC!</h2>';
        }
        ?>
        <h2>Przelew w HomePay.pl</h2>
        <form method="POST" action="inc/Homepay/homepay.php">
            <input type="hidden" name="init_transfer" value="1">
            <div class="form-group">
                <label for="amount">Kwota doładowania</label>
                <input type="number" name="amount" id="amount" value="" style="height: 25px;">
            </div>

            <div class="form-group">
                <label for="name">Imię</label>
                <input type="text" name="name" value="" id="name">
            </div>

            <div class="form-group">
                <label for="surname">Nazwisko</label>
                <input type="text" name="surname" value="" id="surname">
            </div>
            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="text" name="email" value="" id="email">
            </div>
            <input type="submit" value="Dalej">
        </form>

        <h2>Sms</h2>
        <div id="alert3"></div>
        <table class="table table-striped" ng-init="GetSmsApi()">
            <thead>
            <tr>
                <th>Wybierz</th>
                <th>Kwota doładowania portfela</th>
                <th>Cena</th>
                <th>Numer</th>
                <th>Treść smsa</th>
            </tr>
            </thead>
            <tbody>
            <form method="post" id="checkSms">
                <tr ng-repeat="service in services">
                    <td>
                        <input type="radio" name="id_api" value="{{service.id}}" class="id_api">
                    </td>
                    <td>{{service.amount}}zł</td>
                    <td>{{ service.price }}zł</td>
                    <td>{{service.number}}</td>
                    <td>{{service.code}}</td>
                </tr>
                <tr>
                    <td>Wpisz kod zwrotny:</td>
                    <td colspan="4"><input type="text" id="check" name="check"> <button class="btn btn-success" ng-click="CheckCode()">Wyślij!</button></td>
                </tr>
            </form>
            </tbody>
        </table>

        <h2>PaySafeCard</h2>
        <form action="PhpScript/Homepay/homepay.php" method="post">
            <label for="amount">Kwota (w zł)</label>
            <input type="number" name="amount" id="amount"  style="height: 25px;">

            <br/>
            <input type="submit" value="Dalej">
        </form>
    </div>
</section>
