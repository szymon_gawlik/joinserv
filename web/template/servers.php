<div class="container-fluid" ng-controller="GetServers">
    <div>
        <form>
            <div id="alert"></div>
            <div class="form-group">
                <label for="ip-adress">Ip: </label>
                <input type="text" id="ip-adress">
                <button class="btn btn-success" ng-click="AddServer()"><span class="glyphicon glyphicon-plus"></span> Dodaj</button>
            </div>

        </form>
    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>L.p</th>
                <th>Nazwa serwera</th>
                <th>Mapa</th>
                <th>Adres Ip</th>
                <th>Sloty</th>
            </tr>
        </thead>
        <tbody>
        <tr ng-repeat="server in newData">
            <td>{{ $index+1 }}</td>
            <td>{{ server.info.HostName }}</td>
            <td>{{ server.info.Map }}</td>
            <td>{{ server.adress }}</td>
            <td>{{ server.info.Players }} / {{ server.info.MaxPlayers }}</td>
        </tr>
        </tbody>
    </table>
</div>