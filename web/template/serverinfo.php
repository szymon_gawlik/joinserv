<section ng-controller="GetServers" ng-init="Info('<?php echo $_GET['ip'] ?>')">
    <div id="motd" ng-if="error.error !== true">
        <h1>Informacje o serwerze</h1>
        <table class="table table-striped">
            <tr>
                <td>Nazwa serwera</td>
                <td>{{ server.info.HostName }}</td>
                <th>Aktualna mapa</th>
            </tr>
            <tr>
                <td>Ip</td>
                <td>{{ server.ip }}</td>
                <td rowspan="6" ng-if="server.map">
                    <img src="web/uploaded/maps/{{server.info.Map}}.{{server.extension}}">
                </td>
                <td rowspan="6" ng-if="!server.map">
                    Brak miniatury!<br> Możesz ją dodać w swoim panelu!
                </td>

            </tr>
            <tr>
                <td>Mapa</td>
                <td>{{ server.info.Map }}</td>
            </tr>
            <tr>
                <td>Gracze</td>
                <td>{{ server.info.Players }} / {{ server.info.MaxPlayers }}</td>
            </tr>
            <tr>
                <td>Typ</td>
                <td>{{server.type}}</td>
            </tr>
            <tr>
                <td>Gra</td>
                <td>{{ server.info.ModDesc }}</td>
            </tr>
            <tr>
                <td>Właściciel</td>
                <td>{{ server.owner }}</td>
            </tr>

        </table>
        <br><br><br><br>
        <h2>Lista graczy:</h2><br><br>
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>L.p</th>
                    <th>Nazwa</th>
                    <th>Fragi</th>
                    <th>Czas online</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="player in server.players" style="text-align: center">
                    <td>{{ $index+1 }}</td>
                    <td>{{player.Name}}</td>
                    <td>{{player.Frags}}</td>
                    <td>{{player.TimeF}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="motd" ng-if="error.error === true">
        <h2>{{ error.message }}</h2>
    </div>
</section>