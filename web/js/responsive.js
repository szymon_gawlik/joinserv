var WindowWidth = 0;

$(document).ready(function() {
	WindowResize();
	WindowWidth = $(window).width();
	$(window).resize( function() {
		WindowResize();
	});
});
function WindowResize() {
	if(WindowWidth != $(window).width()) {	
			if($(window).width() <= 600)
				$("#sidebar").hide();
			else 
				$("#sidebar, #content").show();
			
			WindowWidth = $(window).width();
		}
}

function toggleSidebar() {
	$("#sidebar").slideToggle(200);
	$("#content").slideToggle(200);
}