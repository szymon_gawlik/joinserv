$(document).ready(function(){
	$(".toggle").click(function(){
		if($('#' + $(this).attr('toggle')).css('display') == 'none')
			$(this).addClass('nav-active');

		else
			$(this).removeClass('nav-active');
		
		$('#' + $(this).attr('toggle')).slideToggle(320, "swing");
	});
});
