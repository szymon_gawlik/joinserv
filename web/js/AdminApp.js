var app = angular.module('AdminApp',['ngRoute','ngDialog','ui.bootstrap']);

app.controller('MainCtrl',function($rootScope,$scope,$http,ngDialog){
    $http.get('../inc/admin/GetUsers.php').success(function(data){
        $rootScope.users = angular.fromJson(data);

        $scope.currentPage = 1;
        $scope.numPerPage = 50;
        $scope.maxSize = 10;

        $scope.numPages = function () {
            return Math.ceil($rootScope.users.length / $scope.numPerPage);
        };

        $scope.$watch('currentPage + numPerPage', function() {
            var begin = (($scope.currentPage - 1) * $scope.numPerPage),
                end = begin + $scope.numPerPage;
            $scope.filteredUsers = $rootScope.users.slice(begin, end);
        });

        $scope.permissions = [];
        $scope.permissions[0] = 'Użytkownik';
        $scope.permissions[1] = 'Redaktor';
        $scope.permissions[2] = 'Administrator';
    });

    $scope.DeleteUser = function (id,index) {
        $.ajax({
            url: '../inc/admin/DeleteUser.php',
            data: {id:id},
            type: 'post'
        }).success(function(data){
            var newData = angular.fromJson(data);

            if(newData.error === false){
                addAlert('success','Poprawnie usunięto!',0);
                $scope.$apply(function(){
                    $scope.users.splice(index,1);
                })
            } else {
                addAlert('error', newData.error,0)
            }
        });
    };

    $scope.OpenDialogPermission = function (id,index) {
        $scope.userPermission = $scope.users[index];
        if($scope.userPermission.admin === 0){
            $scope.user = 'selected'
        } else if ($scope.userPermission.admin === 1) {
            $scope.redaktor = 'selected'
        }else if ($scope.userPermission.admin === 2) {
            $scope.admin = 'selected'
        }
        ngDialog.open({
            template: '../web/template/admin/users/permission.html',
            scope: $scope
        });
    };

    $scope.ChangePermission = function (id) {
        var permission = $('#permission').val();

        $.ajax({
            url: '../inc/admin/ChangePermission.php',
            data: {id: id, permission: permission},
            type: 'post'
        }).success(function (data) {
            var newData = angular.fromJson(data);

            if (newData.error === false) {
                addAlert('success', newData.message, 2);
                location.reload()
            } else {
                addAlert('error', newData.error, 2)
            }
        });
    };

    $scope.OpenDialogWallet = function (id,index) {
        $scope.userWallet = $scope.users[index];

        ngDialog.open({
            template: '../web/template/admin/users/wallet.html',
            scope: $scope
        });
    };

    $scope.FillWallet = function (id) {
        var amount = $('#amount').val();

        $.ajax({
            url: '../inc/admin/FillWallet.php',
            data: {id: id, amount:amount},
            type: 'post'
        }).success(function (data) {
            var newData = angular.fromJson(data);

            if (newData.error === false) {
                addAlert('success', newData.message, 2);
                location.reload()
            } else {
                addAlert('error', newData.error, 2)
            }
        });
    };

    $scope.GetSmsApi = function () {
        $.ajax({
            url: '../inc/get/GetSmsApi.php'
        }).success(function(data){
            $scope.$apply(function () {
                $scope.services = angular.fromJson(data);
            });
        });
    };

    $scope.EditSms = function (type,index) {
        var template;
        if(type === 'add') {
            template = 'AddSms.html';
        } else {
            template = 'EditSms.html';
        }
        $scope.sms = $scope.services[index];
        ngDialog.open({
            template: '../web/template/admin/homepay/' + template,
            scope: $scope
        })
    };

    $scope.EditSmsAccept = function (action) {
        var form = document.getElementById(action),
            data = new FormData(form);

        $.ajax({
            url: '../inc/admin/'+action+'.php',
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                var newData = angular.fromJson(data);

                if (newData.error === false) {
                    addAlert('success', newData.message, action);
                    if(!newData.reload) {
                        location.reload()
                    }
                } else {
                    addAlert('error', newData.error, action)
                }
            }
        });
    };

    $scope.DeleteSms = function (id,index) {
        $.ajax({
            url: '../inc/admin/DeleteSms.php',
            data: {id:id},
            type: 'post'
        }).success(function(data){
            var newData = angular.fromJson(data);

            if(newData.error === false){
                addAlert('success','Poprawnie usunięto!',0);
                $scope.$apply(function(){
                    $scope.services.splice(index,1);
                })
            } else {
                addAlert('error', newData.error,0)
            }
        });
    };
});

app.controller('AddItem',function($scope){
    $scope.AddNews = function(){
        var subject = $("#subject").val(),
            content = $("textarea#content").val();

        $.ajax({
            url: '../inc/admin/AddNews.php',
            data: {subject:subject,content:content},
            type: 'post'
        }).success(function(data){
             var newData = angular.fromJson(data);

            if(newData.error === false){
                addAlert('success',newData.message,0)
            } else {
                addAlert('error', newData.error,0)
            }
        });
    };

});

app.controller('EditItem',function($scope,$http, ngDialog){
    $scope.GetNews = function () {
        $http.get('../inc/admin/GetNews.php').success(function(data){
            $scope.news = angular.fromJson(data);
            $scope.currentPage = 1;
            $scope.numPerPage = 50;
            $scope.maxSize = 10;

            $scope.numPages = function () {
                return Math.ceil($scope.news.length / $scope.numPerPage);
            };

            $scope.$watch('currentPage + numPerPage', function() {
                var begin = (($scope.currentPage - 1) * $scope.numPerPage),
                    end = begin + $scope.numPerPage;
                $scope.filteredNews = $scope.news.slice(begin, end);
            });
        })
    };

    $scope.DeleteNews = function (id,index) {
        $.ajax({
            url: '../inc/admin/DeleteNews.php',
            data: {id:id},
            type: 'post'
        }).success(function(data){
            var newData = angular.fromJson(data);

            if(newData.error === false){
                addAlert('success','Poprawnie usunięto!');
                $scope.$apply(function(){
                    $scope.news.splice(index,1);
                })
            } else {
                addAlert('error', newData.error)
            }
        });
    };

    $scope.EditNews = function (index) {
        $scope.newsToEdit = $scope.news[index];
        ngDialog.open({
            template: '../web/template/admin/news/EditNews.html',
            scope: $scope
        })
    };

    $scope.AcceptEditNews = function (id) {
        var subject = $("#subject").val(),
            content = $("textarea#content").val();

        $.ajax({
            url: '../inc/admin/EditNews.php',
            data: {subject:subject,content:content,id:id},
            type: 'post'
        }).success(function(data){
            var newData = angular.fromJson(data);

            if(newData.error === false){
                addAlert('success',newData.message,1);
                location.reload();
            } else {
                addAlert('error', newData.error,1)
            }
        });
    };

});

app.controller('Servers',function($rootScope,$scope,$http){
    $scope.GetServers = function () {
        $http.get('../inc/get/GetServers.php').success(function(data){
            $scope.servers = angular.fromJson(data);
            var i=0;
            $scope.servers.forEach(function(server){
                var login;
                $rootScope.users.forEach(function(user){
                    if(user.id === server.owner){
                        login = user.login;
                    }
                });

                $scope.servers[i].owner = login;
                i++;
            });
            $scope.currentPage = 1;
            $scope.numPerPage = 50;
            $scope.maxSize = 10;

            $scope.numPages = function () {
                return Math.ceil($scope.servers.length / $scope.numPerPage);
            };

            $scope.$watch('currentPage + numPerPage', function() {
                var begin = (($scope.currentPage - 1) * $scope.numPerPage),
                    end = begin + $scope.numPerPage;
                $scope.filteredServers = $scope.servers.slice(begin, end);
            });
        });
    };

    $scope.DeleteServer = function (id,index,action) {
        $.ajax({
            url: '../inc/admin/DeleteServer.php',
            data: {id:id,action:action},
            type: 'post'
        }).success(function(data){
            var newData = angular.fromJson(data);

            if(newData.error === false){
                addAlert('success','Poprawnie usunięto!',0);
                $scope.$apply(function(){
                    $scope.servers.splice(index,1);
                })
            } else {
                addAlert('error', newData.error,0)
            }
        });
    };

    $scope.GetNetworks = function () {
        $http.get('../inc/get/GetServersNetwork.php').success(function(data){
                $scope.networks = angular.fromJson(data);
                var i=0;
                $scope.networks.forEach(function(server) {
                    var login;
                    $rootScope.users.forEach(function (user) {
                        if (user.id === server.owner) {
                            login = user.login;
                        }
                    });

                    $scope.networks[i].owner = login;
                    i++;
            });
            $scope.currentPage = 1;
            $scope.numPerPage = 50;
            $scope.maxSize = 10;

            $scope.numPages = function () {
                return Math.ceil($scope.networks.length / $scope.numPerPage);
            };

            $scope.$watch('currentPage + numPerPage', function() {
                var begin = (($scope.currentPage - 1) * $scope.numPerPage),
                    end = begin + $scope.numPerPage;
                $scope.filteredNetwork = $scope.networks.slice(begin, end);
            });
        });
    };

    $scope.DeleteNetwork = function (id,index) {
        $.ajax({
            url: '../inc/admin/DeleteServer.php',
            data: {id:id,network:true},
            type: 'post'
        }).success(function(data){
            var newData = angular.fromJson(data);

            if(newData.error === false){
                addAlert('success','Poprawnie usunięto!',0);
                $scope.$apply(function(){
                    $scope.networks.splice(index,1);
                })
            } else {
                addAlert('error', newData.error,0)
            }
        });
    };
});

app.controller('Sites', function ($scope, $http, ngDialog) {
    $http.get('../inc/admin/GetSites.php').success(function (data) {
        $scope.sites = angular.fromJson(data);
    });

    $scope.OpenEditSite = function (index) {
        $scope.siteToEdit = $scope.sites[index];

        ngDialog.open({
            template: '../web/template/admin/sites/edit.html',
            scope: $scope
        });
    };

    $scope.EditSite = function (id) {
        var subject = $("#subject").val(),
            content = $("textarea#content").val();

        $.ajax({
            url: '../inc/admin/EditSite.php',
            data: {subject:subject,content:content,id:id},
            type: 'post'
        }).success(function(data){
            var newData = angular.fromJson(data);

            if(newData.error === false){
                addAlert('success',newData.message,1);
                location.reload();
            } else {
                addAlert('error', newData.error,1)
            }
        });
    }


});

app.controller('Homepay', function($scope){
    $.ajax({
        url: '../inc/admin/HomepaySettings.php'
    }).success(function (data) {
        var newData = angular.fromJson(data);

        $scope.$apply(function(){
            $scope.settings = newData[0];
        });
        if (newData.error === false) {


        } else {
            addAlert('error', newData.error, 2)
        }
    });

    $scope.Edit = function () {
        var form = document.getElementById('editSetting'),
            data2 = new FormData(form);
        $.ajax({
            url: '../inc/admin/EditHomepaySettings.php',
            data: data2,
            type: 'POST',
            cache: false,
            processData: false,
            contentType: false,
            success:function (data) {
                var newData = angular.fromJson(data);
                console.log(data);

                if (newData.error === false) {
                    location.reload();
                } else {
                    addAlert('error', newData.error, 2)
                }
            }
        });
    };
});

app.controller('Pricing', function ($scope, ngDialog) {
    $.ajax({
        url: '../inc/admin/GetPricing.php'
    }).success(function (data) {
        var newData = angular.fromJson(data);

        $scope.$apply(function(){
            $scope.pricing = newData;
        });
    });
    $scope.OpenDialogPrice = function (index,services) {
        $scope.priceEdit = $scope.pricing[index];
        $scope.services = services;

        if(services == 'promo') {
            $scope.price = 'Minimalna cena';
            $scope.expire = 'Minimalny czas trwania';
        } else {
            $scope.price = 'Cena';
            $scope.expire = 'Czas trwania';
        }

        ngDialog.open({
            template: '../web/template/admin/prices/dialog.html',
            scope: $scope
        });
    };

    $scope.EditPrice = function (id) {
        var price = $('#price').val(),
            expire = $('#expire').val();

        $.ajax({
            url: '../inc/admin/EditPrice.php',
            data: {id: id, price: price,expire:expire},
            type: 'post'
        }).success(function (data) {
            var newData = angular.fromJson(data);

            if (newData.error === false) {
                addAlert('success', newData.message, 2);
                location.reload()
            } else {
                addAlert('error', newData.error, 2)
            }
        });
    };

});

app.controller('Maps', function ($scope,$http) {
    $http.get('../inc/admin/GetMaps.php').success(function (data) {
        $scope.maps = angular.fromJson(data);
    });

    $scope.Delete = function (id, index) {
        $.ajax({
            url: '../inc/admin/EditMaps.php',
            data: {id:id,action:'delete'},
            type: 'post'
        }).success(function(data){
            var newData = angular.fromJson(data);

            if(newData.error === false){
                addAlert('success','Poprawnie usunięto!',0);
                $scope.$apply(function(){
                    $scope.maps.splice(index,1);
                })
            } else {
                addAlert('error', newData.error,0)
            }
        });
    };

    $scope.Accept = function (id, index) {
        $.ajax({
            url: '../inc/admin/EditMaps.php',
            data: {id:id,action:'accept'},
            type: 'post'
        }).success(function(data){
            var newData = angular.fromJson(data);

            if(newData.error === false){
                addAlert('success','Poprawnie zaakceptowano!',0);
                $scope.$apply(function(){
                    $scope.maps.splice(index,1);
                })
            } else {
                addAlert('error', newData.error,0)
            }
        });
    }
});

app.controller('Reviews', function ($scope,$http) {
    $http.get('../inc/get/GetReviews.php').success(function (data) {
        $scope.reviews = angular.fromJson(data);
    });

    $scope.Delete = function (id, index) {
        $.ajax({
            url: '../inc/admin/EditReviews.php',
            data: {id:id,action:'delete'},
            type: 'post'
        }).success(function(data){
            var newData = angular.fromJson(data);

            if(newData.error === false){
                addAlert('success','Poprawnie usunięto!',0);
                $scope.$apply(function(){
                    $scope.reviews.splice(index,1);
                })
            } else {
                addAlert('error', newData.error,0)
            }
        });
    };

    $scope.Accept = function (id, index) {
        $.ajax({
            url: '../inc/admin/EditReviews.php',
            data: {id:id,action:'accept'},
            type: 'post'
        }).success(function(data){
            var newData = angular.fromJson(data);

            if(newData.error === false){
                addAlert('success','Poprawnie zaakceptowano!',0);
                $scope.$apply(function(){
                    $scope.reviews.splice(index,1);
                })
            } else {
                addAlert('error', newData.error,0)
            }
        });
    }
});

app.controller('Mailer', function ($scope) {
    $scope.SendMail = function () {
        var subject = $("#subject").val(),
            content = $("textarea#content").val();

        $.ajax({
            url: '../inc/admin/Mailer.php',
            data: {subject:subject,content:content},
            method: 'post'
        }).success(function (data) {
            var newData = angular.fromJson(data);

            if (newData.error === false) {
                addAlert('success', newData.message, 2)
            } else {
                addAlert('error', newData.error, 2)
            }
        });
    }
});
function addAlert(type, message,id) {
    if(type == 'error') {
        return $('#alert'+id).html('<div class="alert alert-danger"><span class="glyphicon glyphicon-remove-circle"></span> Błąd! '+ message +'</div>');
    } else if(type == 'success') {
        return $('#alert'+id).html('<div class="alert alert-success"><span class="glyphicon glyphicon-ok-circle"></span> ' + message + '</div>');
    }
}

app.directive('ngConfirmClick', [
    function(){
        return {
            link: function ($scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click',function (event) {
                    if ( window.confirm(msg) ) {
                        $scope.$eval(clickAction)
                    }
                });
            }
        };
    }]);
