var app = angular.module('App', ['ngRoute','ngDialog','ui.bootstrap']);

app.controller('MainCtrl',function($rootScope,$scope,$http,ngDialog){

    $http.get('inc/fetchMainData.php').success(function(data){
        var newData = angular.fromJson(data);
            $rootScope.News = newData.news;
            $rootScope.login = newData.login[0];
    });

    $scope.GetSite = function (site) {
        $.ajax({
            url: 'inc/get/GetSite.php',
            type: 'get',
            data: {site:site}
        }).success(function(data){
            $scope.$apply(function () {
                $scope.Site = angular.fromJson(data)[0];
            });
        });
    };

    $scope.Login = function () {
        var login = $('#login').val(),
            pw = $('#login_pw').val();

        $.ajax({
            url: 'inc/Login.php',
            type: 'post',
            data: {login:login,pw:pw}
        }).success(function(data){
            var newData = angular.fromJson(data);

            if(newData.error === false) {
                location.reload()
            } else {
                addAlert('error',newData.error,'login');
            }
        });
    };

    $scope.OpenRegister = function () {
        ngDialog.open({
            template: 'web/template/register.html',
            scope: $scope
        });
    };

    $scope.OpenPromo = function () {
        ngDialog.open({
            template: 'web/template/promo.html',
            scope: $scope
        });
    };

    $scope.Stawki = function () {
        $http.get('inc/Stawki.php').success(function(data){
            $scope.stawki = angular.fromJson(data);
        });
    };

    $scope.SendForm = function (action) {
        var form = document.getElementById(action),
            data = new FormData(form);

        $.ajax({
            url: 'inc/'+action+'.php',
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                var newData = angular.fromJson(data);

                if (newData.error === false) {
                    addAlert('success', newData.message, action);
                    if(!newData.reload) {
                        location.reload()
                    }
                    if(newData.reload) {
                        setTimeout(function () {
                            location.href = newData.reload;
                        },2000)
                    }
                } else {
                    addAlert('error', newData.error, action)
                }
            }
        });
    };

    $scope.Reviews = function () {
         $.ajax({
             url: 'inc/get/GetReviews.php'
         }).success(function (data) {
             $scope.reviews = angular.fromJson(data);
         });
    };

    $scope.CheckReview = function () {
        $.ajax({
            url: 'inc/get/GetReviews.php',
            data: {login:'true'},
            type: 'post'
        }).success(function (data) {
            var newData = angular.fromJson(data);
            $scope.$apply(function () {
                $scope.Visability = newData['if'];
                $scope.review = newData['data'];

            })
        });

    };

    $scope.EditReview = function () {
        var content = $('textarea#content1').val(),
            id = $('#id').val();

        $.ajax({
            url: 'inc/EditReview.php',
            data: {id: id, content:content},
            type: 'post'
        }).success(function (data) {
            var newData = angular.fromJson(data);

            if (newData.error === false) {
                addAlert('success', newData.message, 2);
                location.reload()
            } else {
                addAlert('error', newData.error, 2)
            }
        });
    };

    $scope.GetServerNetwork = function () {
        $.ajax({
            url: 'inc/get/GetServersNetwork.php',
            type: 'post',
            data: {servers: $rootScope.servers, id:$rootScope.login.id}
        }).success(function(data){
            var newData = angular.fromJson(data);
            $scope.$apply(function () {
                $scope.networks = newData;
            })
        });
    };

    $scope.GetSmsApi = function () {
        $.ajax({
            url: 'inc/get/GetSmsApi.php'
        }).success(function(data){
            $scope.$apply(function () {
                $scope.services = angular.fromJson(data);
            });
        });
    };

    $scope.CheckCode = function () {
        var code = $("#check").val(),
            id = $(".id_api:checked").val();

        $.ajax({
            url: 'inc/Homepay/SmsApi.php',
            type: 'post',
            data: {check:code,id:id}
        }).success(function(data){
            var newData = angular.fromJson(data);

            if (newData.error === false) {
                addAlert('success', newData.message, 3);
                if(!newData.reload) {
                    location.reload()
                }
            } else {
                addAlert('error', newData.error, 3)
            }
        });
    };

    $scope.GetCircleInfo = function () {
        $http.get('inc/CircleInfo.php').success(function (data) {
            var newData = angular.fromJson(data);
            $scope.ourServers = newData.servers;
            $scope.ourUsers = newData.users;
            $scope.ourNetworks = newData.networks;
        })
    };

    $scope.CheckLogin = function () {
        $http.get('inc/CheckIfLogin.php').success(function (data) {
            var data1 = angular.fromJson(data);

            if(!data1.login ){
                window.location = 'index.php';
            }
        })
    }
});

app.controller('GetServers',function($scope, $rootScope, $http){
    $scope.GetServers = function (type) {
        $.ajax({
            url: 'inc/get/GetServers.php',
            type: 'post',
            data: {type:type}
        }).success(function (data) {
            $scope.$apply(function(){
                $rootScope.servers = angular.fromJson(data);
                $scope.filteredTodos = [];
                $scope.currentPage = 1;
                $scope.numPerPage = 20;
                $scope.maxSize = 5;

                $scope.numPages = function () {
                    return Math.ceil($rootScope.servers.length / $scope.numPerPage);
                };

                $scope.$watch('currentPage + numPerPage', function() {
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage),
                        end = begin + $scope.numPerPage;
                    $scope.filteredServers = $rootScope.servers.slice(begin, end);
                });
            });
        });
    };

    $scope.ProgressBar = function(player,maxplayers) {
        var percentage = (player/maxplayers)*100,
            background = '#009C2D';
        if(percentage > 100) {
            percentage = 100;
        }
        if(percentage < 33) {
            background = '#C80000';
        } else if(percentage < 66) {
            background = '#B1D400';
        }
        return ({width:percentage+'%',background:background})
    };

    $scope.Info = function(ip) {
        $.ajax({
            url: 'inc/get/GetServerInfo.php',
            type: 'get',
            data: {ip:ip}
        }).success(function(data){
            var newData = angular.fromJson(data);

                $scope.$apply(function () {
                    if(newData.error) {
                        $scope.error = newData;
                    } else {
                        $scope.server = newData[0];
                    }
                });
        });
    };

    $scope.GetTypeServer = function () {
        $.ajax({
            url: 'inc/get/GetTypeServer.php'
        }).success(function(data){
            var newData = angular.fromJson(data);

            $scope.$apply(function () {
                $scope.types = newData;
            })
        });
    };

    $scope.HighLight = function (data) {
        if(data !== null) {
            if(data == 1) {
                return ({textDecoration:'underline'});
            }
        }
    };

    $scope.SetUnderline = function () {
        var ip = $("#serverIP").val();

        $.ajax({
            url: 'inc/SetUnderline.php',
            type: 'post',
            data: {ip:ip}
        }).success(function (data) {
            var newData = angular.fromJson(data);

            if (newData.error === false) {
                location.reload();
            } else {
                alert(newData.error);
            }
        });
    };

    $scope.SetPromo = function () {
        var ip = $("#serverIPPromo").val();
        $.ajax({
            url: 'inc/SetPromo.php',
            type: 'post',
            data: {ip:ip}
        }).success(function (data) {
            var newData = angular.fromJson(data);

            if (newData.error === false) {
                location.reload();
            } else {
                alert(newData.error);
            }
        });
    };

    $scope.PriceOfUnderline = function () {
        $http.get('inc/get/PriceOfUnderline.php').success(function (data) {
            $scope.underlinePrice = data;
        });
    }

});

app.controller('Paginate', function($rootScope,$scope,$http){
    $scope.filteredTodos = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 5;
    $scope.maxSize = 5;

    $http.get('inc/fetchMainData.php').success(function(data) {
        var newData = angular.fromJson(data);
        $rootScope.News = newData.news;
        $scope.numPages = function () {
            return Math.ceil($rootScope.News.length / $scope.numPerPage);
        };

        $scope.$watch('currentPage + numPerPage', function() {
            var begin = (($scope.currentPage - 1) * $scope.numPerPage),
                end = begin + $scope.numPerPage;
            $scope.filteredTodos = $rootScope.News.slice(begin, end);
        });
    });
});

app.filter('trusted', function ($sce) {
    return function (input) {
        return $sce.trustAsHtml(input)
    }
});
function addAlert(type, message,id) {
    if(type == 'error') {
        return $('#alert'+id).html('<div class="alert alert-danger"><span class="glyphicon glyphicon-remove-circle"></span> Błąd! '+ message +'</div>');
    } else if(type == 'success') {
        return $('#alert'+id).html('<div class="alert alert-success"><span class="glyphicon glyphicon-ok-circle"></span> ' + message + '</div>');
    }
}