<?php
require_once '../Class/MyAutoloader.php';
include_once '../vendor/autoload.php';

use Database\SelectFromDb;
use Database\InsertToDb;
use Symfony\Component\HttpFoundation\Request;

try {
    $request = Request::createFromGlobals();
    $subject = $request->request->get('subject');
    $content = $request->request->get('content');

    if(empty($subject) || empty($content)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }
    $date = (new Datetime('NOW', new DateTimeZone('Europe/Warsaw')))->format('d.m.Y H:i:s');
    if(new InsertToDb(array($subject,$content,$date),'serwery_news')) {
        $newData = (new SelectFromDb('serwery_news',array('subject','content','date')))->result;
        $array = array(
            'error' => false,
            'data' => $newData
        );
        echo json_encode($array);
    }

} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}