<?php
require_once '../Class/MyAutoloader.php';
include_once '../vendor/autoload.php';


use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Session\Session;

try {
    $session = new Session();
    $fetch = array();
    $data = (new SelectFromDb('news',array('subject','content','date')))->result;
        if($session->get('LoginId')) {
            $login = (new SelectFromDb('accounts',array('login','admin','wallet','avatar'),array('id'=>$session->get('LoginId')),1))->result;
            $fetch['login'] = $login;
        }
        $i=0;
        foreach($data as $new ) {
            $data[$i]['content'] = htmlspecialchars_decode($new['content']);
            $i++;
        }
        $fetch['news'] = array_reverse($data);

    echo json_encode($fetch);
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}