<?php
require_once '../Class/MyAutoloader.php';
include_once '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\InsertToDb;
use Symfony\Component\HttpFoundation\Session\Session;

try {
    $session = new Session();
    $request = Request::createFromGlobals();
    $name = input($request->request->get('name'));
    $website = input($request->request->get('website'));
    if(empty($name)){
        echo '{"error":"Wypełnij pole <strong>Nazwa!</strong>"}';
        exit;
    }
    $date = (new DateTime('now', new DateTimeZone('Europe/Warsaw')))->format('d.m.Y H:i:s');

    if(new InsertToDb(array($name,$date,0,$website,$session->get('LoginId')),'servers_network')){
        echo '{"error":false, "message":"Poprawnie dodano sieć serwerów!"}';
    }

} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}