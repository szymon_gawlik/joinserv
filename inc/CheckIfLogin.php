<?php
require_once '../Class/MyAutoloader.php';
include_once '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Session\Session;

try {
    $session = new Session();

    echo ($session->get('LoginId') && $session->get('Permission')) ? '{"login":true}' : '{"login":false}';

} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}