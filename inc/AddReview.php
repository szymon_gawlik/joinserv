<?php
require_once '../Class/MyAutoloader.php';
include_once '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\InsertToDb;

try {
    $request = Request::createFromGlobals();
    $name = input($request->get('name'));
    $content = input($request->get('content'));

    if(empty($content) || empty($name)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }
    $date = (new DateTime('now', new DateTimeZone('Europe/Warsaw')))->format('d.m.Y H:i:s');

    if(new InsertToDb(array($name,nl2br($content),$date,0),'reviews')){
        echo '{"error":false, "message":"Poprawnie dodano opinie! Dziękujemy!"}';
    }

}catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}
