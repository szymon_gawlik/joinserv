<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';


use Database\SelectFromDb;

try {
    if($data = (new SelectFromDb('news',array('subject','content','date','edit')))->result){
        $i=0;
        foreach($data as $new ) {
            $data[$i]['content'] = htmlspecialchars_decode($new['content']);
            $i++;
        }
        $fetch = array_reverse($data);
        echo json_encode($fetch);
    }
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}