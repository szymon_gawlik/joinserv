<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';


use Database\Delete;
use Database\UpdateDb;
use Symfony\Component\HttpFoundation\Request;

try {
    $request = Request::createFromGlobals();
    $id = input($request->request->get('id'));
    $network = $request->request->get('network');
    $action = $request->request->get('action');

    switch($action) {
        case 'server':
            if($network) {
                if ($data = new Delete('servers_network', $id)) {
                    echo '{"error":false}';
                }
            } else {
                if ($data = new Delete('servers', $id)) {
                    echo '{"error":false}';
                }
            }
            break;

        case 'promo':
            new UpdateDb(array('promo'=>0,'promo_expire'=>0,'position'=>0,'promo_amount'=>0),'servers',$id);
            echo '{"error":false}';
            break;
    }


} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}