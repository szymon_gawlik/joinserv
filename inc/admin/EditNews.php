<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\UpdateDb;
$request = Request::createFromGlobals();

try {
    $subject = input($request->request->get('subject'));
    $content = input($request->request->get('content'));
    $id = $request->request->get('id');

    if(empty($subject) || empty($content)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }
    $date = (new Datetime('NOW', new DateTimeZone('Europe/Warsaw')))->format('d.m.Y H:i:s');
    if(new UpdateDb(array('subject'=>$subject, 'content' => $content, 'edit' => $date),'news',$id)){
        echo '{"error":false, "message":"Poprawnie edytowano newsa!"}';
    }
} catch (Exception $e){
    echo '{"error":"'.$e->getMessage().'"}';
}