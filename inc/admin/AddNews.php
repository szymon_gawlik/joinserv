<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\InsertToDb;
$request = Request::createFromGlobals();

try {
    $subject = input($request->request->get('subject'));
    $content = input($request->request->get('content'));

    if(empty($subject) || empty($content)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }
    $date = (new Datetime('NOW', new DateTimeZone('Europe/Warsaw')))->format('d.m.Y H:i:s');
    if(new InsertToDb(array($subject,$content,$date,0),'news')){
        echo '{"error":false, "message":"Poprawnie dodano newsa!"}';
    }
} catch (Exception $e){
    echo '{"error":"'.$e->getMessage().'"}';
}