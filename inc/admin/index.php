<?php
include_once '../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\RedirectResponse;

$redirect = new RedirectResponse('../index.php');
$redirect->send();