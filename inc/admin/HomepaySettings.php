<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';


use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Session\Session;

try {
    if($data = (new SelectFromDb('homepay_settings',array('id_user','acc_id','acc_hash','private_key','public_key'),array('id'=>1),1))->result){
        echo json_encode($data);
    }
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}