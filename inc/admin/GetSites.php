<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\SelectFromDb;

try {
    if($sites = (new SelectFromDb('sites',array('subject','content','site')))->result){
        echo json_encode($sites);
    } else echo '{"error":"Brak strony o podanym adresie!"}';

}catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}