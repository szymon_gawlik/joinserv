<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\UpdateDb;
$request = Request::createFromGlobals();

try {
    $permission = input($request->request->get('permission'));
    $id = $request->request->get('id');

    if(empty($permission)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }
    if(new UpdateDb(array('admin'=>$permission),'accounts',$id)){
        echo '{"error":false, "message":"Poprawnie zmieniono uprawnienia!"}';
    }
} catch (Exception $e){
    echo '{"error":"'.$e->getMessage().'"}';
}