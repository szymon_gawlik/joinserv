<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\UpdateDb;
$request = Request::createFromGlobals();

try {
    $id = input($request->request->get('id'));
    $acc_id = input($request->request->get('acc_id'));
    $hash = input($request->request->get('hash'));
    $public_key = input($request->request->get('public_key'));
    $private_key = input($request->request->get('private_key'));

    if(empty($id) || empty($acc_id) || empty($hash)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }

    if(new UpdateDb(array('id_user'=>$id, 'acc_id'=>$acc_id, 'acc_hash'=>$hash,'private_key'=>$private_key,'public_key'=>$public_key),'homepay_settings','1')){
        echo '{"error":false, "message":"Poprawnie edytowano ustawienia!"}';
    }
} catch (Exception $e){
    echo '{"error":"'.$e->getMessage().'"}';
}