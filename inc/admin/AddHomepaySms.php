<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\InsertToDb;
$request = Request::createFromGlobals();

try {
    $sms_id = input($request->request->get('sms_id'));
    $description = input($request->request->get('desc'));
    $price = input($request->request->get('price'));
    $amount = input($request->request->get('amount'));
    $number = input($request->request->get('number'));
    $code = input($request->request->get('code'));

    if(empty($description) || empty($price) || empty($amount) || empty($number) || empty($code)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }

    if(new InsertToDb(array($sms_id,$number,$code,$amount,$description,$price),'sms_api')){
        echo '{"error":false, "message":"Poprawnie dodano newsa!"}';
    }
} catch (Exception $e){
    echo '{"error":"'.$e->getMessage().'"}';
}