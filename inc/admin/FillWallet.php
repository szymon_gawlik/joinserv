<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\UpdateDb;
use Database\SelectFromDb;

$request = Request::createFromGlobals();

try {
    $amount = input($request->request->get('amount'));
    $id = $request->request->get('id');

    if(empty($amount)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }
    $wallet = (new SelectFromDb('accounts',array('wallet'),array('id'=>$id)))->result[0];
    $newAmount = $wallet['wallet']+$amount;
    if(new UpdateDb(array('wallet' => $newAmount),'accounts',$id)){
        echo '{"error":false, "message":"Poprawnie zmieniono uprawnienia!"}';
    }
} catch (Exception $e){
    echo '{"error":"'.$e->getMessage().'"}';
}