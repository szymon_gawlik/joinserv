<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';


use Database\SelectFromDb;

try {
    if($data = (new SelectFromDb('pricing',array('price','service','name','expire')))->result){

        echo json_encode($data);
    }
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}