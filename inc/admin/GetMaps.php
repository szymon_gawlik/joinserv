<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';


use Database\SelectFromDb;

try {
    if($data = (new SelectFromDb('map_image',array('name','extension'),array('accept'=>0)))->result){

        echo json_encode($data);
    }
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}