<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';


use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Session\Session;

try {
    if($data = (new SelectFromDb('accounts',array('login','email','date','admin','wallet')))->result){
        echo json_encode($data);
    }
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}