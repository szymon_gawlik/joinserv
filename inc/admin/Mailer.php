<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\SelectFromDb;

try {
    $request = Request::createFromGlobals();

    $subject = $request->request->get('subject');
    $content = $request->request->get('content');

    if(empty($subject) || empty($content)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }

    $usersEmail = (new SelectFromDb('accounts',array('email')))->result;

    $mailer = Swift_Message::newInstance();
    $mailer->setSubject($subject);
    $mailer->setFrom(array('noreply@joinserv.eu' => 'JoinServ.eu'));
    $mailer->setBody($content,'text/plain');
    $mailer->setPriority(4);

    $mailTransport = Swift_SmtpTransport::newInstance('smtp.gmail.com')
        ->setPort(465)
        ->setEncryption('ssl')
        ->setUsername('sgawlik61@gmail.com')
        ->setPassword('frgorvtwzqjbhxae');

    $mailMailer = Swift_Mailer::newInstance($mailTransport);

    foreach ($usersEmail as $users) {
        $mailer->setTo($users['email']);
        $mailMailer->send($mailer);
    }

    echo '{"error":false,"message":"Poprawnie wysłano maile!"}';
}catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}
