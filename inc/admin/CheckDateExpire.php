<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';


use Database\SelectFromDb;
use Database\UpdateDb;
use Symfony\Component\HttpFoundation\Request;

try {
    $request = Request::createFromGlobals();
    print_r($request->getBasePath());

    $dates = (new SelectFromDb('servers',array('promo_expire','highlight_expire')))->result;
    $now = (new DateTime('now', new DateTimeZone('Europe/Warsaw')))->format('d.m.Y H:i:s');
    $explodeNowDate = explode(' ',$now);
    $nowDate = explode('.',$explodeNowDate[0]);
    $nowTime = explode(':',$explodeNowDate[1]);

    $mktimeNow = mktime($nowTime[0],$nowTime[1],$nowTime[2],$nowDate[1],$nowDate[0],$nowDate[2]);

    foreach ($dates as $date) {
        if($date['promo_expire'] !== 0){
            $explodeDate = explode(' ', $date['promo_expire']);
            $expireDate = explode('.',$explodeDate[0]);
            $expireTime = explode(':',$explodeDate[1]);

            $mktimeExpire = mktime($expireTime[0],$expireTime[1],$expireTime[2],$expireDate[1],$expireDate[0],$expireDate[2]);

            if($mktimeNow > $mktimeExpire) {
                new UpdateDb(array('promo' => 0,'promo_expire' => 0,'promo_amount'=>0),'servers',$date['id']);
            }
        }

        if($date['highlight_expire'] !== 0) {
            $explodeDate = explode(' ', $date['highlight_expire']);
            $expireDate = explode('.',$explodeDate[0]);
            $expireTime = explode(':',$explodeDate[1]);

            $mktimeExpire = mktime($expireTime[0],$expireTime[1],$expireTime[2],$expireDate[1],$expireDate[0],$expireDate[2]);

            if($mktimeNow > $mktimeExpire) {
                new UpdateDb(array('highlight' => 0,'highlight_expire' => 0),'servers',$date['id']);
            }
        }
    }


} catch (Exception $e){

}