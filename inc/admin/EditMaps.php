<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Database\UpdateDb;
use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Request;
use Database\Delete;

try {
    $request = Request::createFromGlobals();
    $id = $request->request->get('id');
    $action = $request->request->get('action');

    if(isset($id)) {
        $map = (new SelectFromDb('map_image',array('name','extension'),array('id'=>$id)))->result[0];

        switch($action) {
            case 'delete':
                unlink("../../web/uploaded/maps/".$map['name'].'.'.$map['extension']);
                if(new Delete('map_image',$id)){
                    echo '{"error":false}';
                }
                break;

            case 'accept':
                if(new UpdateDb(array('accept'=>1),'map_image',$id)){
                    echo '{"error":false}';
                }
                break;
        }
    }
}catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}