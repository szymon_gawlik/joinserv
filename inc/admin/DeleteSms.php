<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';


use Database\Delete;
use Symfony\Component\HttpFoundation\Request;
try {
    $request = Request::createFromGlobals();
    $id = input($request->request->get('id'));
    if($data = new Delete('sms_api',$id)){
        echo '{"error":false}';
    }
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}