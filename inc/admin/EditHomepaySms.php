<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\UpdateDb;
$request = Request::createFromGlobals();

try {
    $id = input($request->request->get('id'));
    $price = input($request->request->get('price'));
    $amount = input($request->request->get('amount'));
    $number = input($request->request->get('number'));
    $code = input($request->request->get('code'));

    if(empty($id) || empty($price) || empty($amount) || empty($number) || empty($code)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }

    if(new UpdateDb(array(
        'price' => $price,
        'amount' => $amount,
        'number' => $number,
        'code' => $code
    ),'sms_api',$id)){
        echo '{"error":false, "message":"Poprawnie edytowano ustawienia!"}';
    }
} catch (Exception $e){
    echo '{"error":"'.$e->getMessage().'"}';
}