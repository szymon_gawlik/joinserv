<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\UpdateDb;
use Database\SelectFromDb;

$request = Request::createFromGlobals();

try {
    $price = input($request->request->get('price'));
    $expire = input($request->request->get('expire'));
    $id = $request->request->get('id');

    if(empty($price) || empty($expire)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }

    if(new UpdateDb(array('price'=>$price,'expire'=>$expire),'pricing',$id)){
        echo '{"error":false, "message":"Poprawnie zmieniono uprawnienia!"}';
    }
} catch (Exception $e){
    echo '{"error":"'.$e->getMessage().'"}';
}