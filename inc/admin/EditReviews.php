<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Database\UpdateDb;
use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Request;
use Database\Delete;

try {
    $request = Request::createFromGlobals();
    $id = $request->request->get('id');
    $action = $request->request->get('action');

        switch($action) {
            case 'delete':
                if(new Delete('reviews',$id)){
                    echo '{"error":false}';
                }
                break;

            case 'accept':
                if(new UpdateDb(array('accept'=>1),'reviews',$id)){
                    echo '{"error":false}';
                }
                break;
        }
}catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}