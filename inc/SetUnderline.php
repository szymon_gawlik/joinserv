<?php
require_once '../Class/MyAutoloader.php';
include_once '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Session\Session;
use Database\UpdateDb;

$session = new Session();
$request = Request::createFromGlobals();

list($ip,$port) = explode(':',input($request->request->get('ip')));
if(empty($ip) && empty($port)) {
    echo '{"error":"Wybierz serwer z listy!"}';
    exit;
}
$price = (new SelectFromDb('pricing',array('price','expire'),array('id'=>1)))->result[0];
$wallet = (new SelectFromDb('accounts',array('wallet'),array('id'=>$session->get('LoginId'))))->result[0];
$date = (new DateTime('+'.$price['expire'].' days', new DateTimeZone('Europe/Warsaw')))->format('d.m.Y H:i:s');

if($price['price'] > $wallet['wallet']) {
    echo '{"error":"Brak środków na koncie!"}';
    exit;
}

try {
    $serverId = (new SelectFromDb('servers',array(),array('ip_adress'=>$ip, 'ip_port' => $port)))->result[0];
    new UpdateDb(array('wallet'=>($wallet['wallet']-$price['price'])),'accounts',$session->get('LoginId'));
    new UpdateDb(array('highlight' => 1,'highlight_expire' => $date),'servers',$serverId['id']);

    echo '{"error":false}';
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}