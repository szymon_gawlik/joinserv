<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Database\SelectFromDb;

try
{
    $types = (new SelectFromDb('server_type',array('name')))->result;
    echo json_encode($types);

}catch( Exception $e )
{
    echo '{"error": "'.$e->getMessage( ).'"}';
}