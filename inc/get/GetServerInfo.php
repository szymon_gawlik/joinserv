<?php
require '../../Class/SourceQuery/SourceQuery.class.php';
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Request;
try
{
    $request = Request::createFromGlobals();
    list($ip,$port) = explode(':',input($request->get('ip')));
    $port = intval($port);
    if(empty($port) || empty($ip)) {
        echo '{"error":true, "message":"Błędny adres ip!"}';
        exit;
    }
    $server = (new SelectFromDb('servers',array('owner','type'),array('ip_adress'=>$ip,'ip_port'=>$port)))->result[0];

    if(empty($server)) {
        echo '{"error":true, "message":"Brak serwera w bazie danych!"}';
        exit;
    }
    $users = (new SelectFromDb('accounts',array('login')))->result;
    $types = (new SelectFromDb('server_type',array('name')))->result;

    foreach ($types as $type) {
        if($type['id'] == $server['type']) {
            $server['type'] = $type['name'];
            break;
        }
    }
    foreach ($users as $user) {
        if($user['id'] == $server['owner']){
            $server['owner'] = $user['login'];
            break;
        }
    }

        // Edit this ->
        $SQ_SERVER_ADDR = $ip;
        $SQ_SERVER_PORT = $port;
        $SQ_TIMEOUT =     1;
        $SQ_ENGINE =      SourceQuery :: SOURCE;
        // Edit this <-

        $Query = new SourceQuery();

        $Query->Connect( $SQ_SERVER_ADDR, $SQ_SERVER_PORT, $SQ_TIMEOUT, $SQ_ENGINE );

        $maps = (new SelectFromDb('map_image',array('accept','extension'),array('name'=>$Query->GetInfo()['Map'])))->result[0];

    if(empty($maps) || $maps['accept'] === 0) {
        $map = false;
    } else {
        $map = true;
    }


        $array[] = [
            'info' => $Query->GetInfo( ),
            'players' => $Query->GetPlayers(),
            'ip' => input($request->get('ip')),
            'owner' => $server['owner'],
            'type' => $server['type'],
            'map' => $map,
            'extension' => $maps['extension']
        ];

    echo json_encode($array);

}
catch( Exception $e )
{
    echo '{"error": "'.$e->getMessage( ).'"}';
}
