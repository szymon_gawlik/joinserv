<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\SelectFromDb;

$request = Request::createFromGlobals();

try {
    $price = (new SelectFromDb('pricing',array('price'),array('id'=>1)))->result[0];

    echo $price['price'];
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}