<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';


use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

try {
    $request = Request::createFromGlobals();
    $login = $request->request->get('login');
    $session = new Session();
    if(empty($login)) {
        if ($data = (new SelectFromDb('reviews', array('name', 'content', 'date','accept')))->result) {
            echo json_encode(array_reverse($data));
        }
    } else {
        $user = (new SelectFromDb('accounts',array('login'),array('id'=>$session->get('LoginId'))))->result[0];
        if ($data = (new SelectFromDb('reviews', array('content'), array('name'=>$user['login'])))->result) {
            $array = ['if'=>false,'data'=>$data[0]];
            echo json_encode($array);
        } else echo '{"if":true}';
    }
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}