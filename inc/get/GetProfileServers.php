<?php
require '../../Class/SourceQuery/SourceQuery.class.php';
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Session\Session;
try
{
    $session = new Session();

    $servers = (new SelectFromDb('servers', array('ip_adress', 'ip_port', 'promo'), array('owner'=>$session->get('LoginId'))))->result;
    $array=[];
    foreach($servers as $server) {
        // Edit this ->
        $SQ_SERVER_ADDR = $server['ip_adress'];
        $SQ_SERVER_PORT = $server['ip_port'];
        $SQ_TIMEOUT =     1;
        $SQ_ENGINE =      SourceQuery :: SOURCE;
        // Edit this <-

        $Query = new SourceQuery( );

        $Query->Connect( $SQ_SERVER_ADDR, $SQ_SERVER_PORT, $SQ_TIMEOUT, $SQ_ENGINE );
        $array[] = [
            'info' => $Query->GetInfo( ),
            'promo' => $server['promo'],
            'ip' => $server['ip_adress'].':'.$server['ip_port']
        ];

    }

    echo json_encode($array);

}
catch( Exception $e )
{
    echo '{"error": "'.$e->getMessage( ).'"}';
}
