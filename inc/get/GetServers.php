<?php
require '../../Class/SourceQuery/SourceQuery.class.php';
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Database\SelectFromDb;
use Database\UpdateDb;
use Symfony\Component\HttpFoundation\Request;

try
{
    $request = Request::createFromGlobals();
    $type = $request->request->get('type');

    switch($type) {
        case 'promo':
            $servers = (new SelectFromDb('servers', array('ip_adress', 'ip_port', 'promo', 'owner','highlight','type','promo_expire','highlight_expire','position','accept'), array('promo' => 1,'accept'=>1)))->result;
            break;
        case 'profile':
            $servers = (new SelectFromDb('servers', array('ip_adress', 'ip_port', 'promo', 'owner','highlight','type','promo_expire','highlight_expire','position','accept')))->result;
            break;
        default:
            $servers = (new SelectFromDb('servers', array('ip_adress', 'ip_port', 'promo', 'owner','highlight','type','promo_expire','highlight_expire','position','accept'), array('accept'=>1)))->result;

    }
    $i=1;
    $array=[];
    foreach($servers as $server) {
        $SQ_SERVER_ADDR = $server['ip_adress']; $SQ_SERVER_PORT = $server['ip_port'];
        $SQ_TIMEOUT =     1;
        $SQ_ENGINE =      SourceQuery :: SOURCE;

        $Query = new SourceQuery( );

        $Query->Connect( $SQ_SERVER_ADDR, $SQ_SERVER_PORT, $SQ_TIMEOUT, $SQ_ENGINE );

        if(strpos($Query->GetInfo()['HostName'], 'JoinServ.eu')) {
            new UpdateDb(array('accept'=>1),'servers',$server['id']);
            $server['accept'] = 1;
        }

        if($Query->Ping()) {
            $array[] = [
                'lp' => $i,
                'info' => $Query->GetInfo(),
                'promo' => $server['promo'],
                'owner' => $server['owner'],
                'hl' => $server['highlight'],
                'ip' => $server['ip_adress'] . ':' . $server['ip_port'],
                'id' => $server['id'],
                'type' => $server['type'],
                'promo_expire' => $server['promo_expire'],
                'highlight_expire' => $server['highlight_expire'],
                'position'=>$server['position'],
                'accept' => $server['accept']
            ];
        }
        $i++;

    }


    echo json_encode($array);

}
catch( Exception $e )
{
    echo '{"error": "'.$e->getMessage( ).'"}';
}
