<?php
require_once '../../Class/MyAutoloader.php';
require_once '../../vendor/autoload.php';


use Database\SelectFromDb;

try {
    if($data = (new SelectFromDb('sms_api',array('number','code','amount','price')))->result) {
        echo json_encode($data);
    }
}catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}