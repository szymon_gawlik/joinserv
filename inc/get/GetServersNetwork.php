<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Request;

$request = Request::createFromGlobals();
$servers = $request->request->get('servers');
$user_id = $request->request->get('id');

try {
    if($networks = (new SelectFromDb('servers_network',array('name','date','server_count','website','owner')))->result){

       /* foreach ($servers as $server) {
            if(strpos($server['info']['HostName'],$networks['name'])) {
                $networks['server_count'] += 1;
            }
        }*/
        echo json_encode($networks);
    }

} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}