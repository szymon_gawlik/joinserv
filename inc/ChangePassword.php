<?php
require_once '../Class/MyAutoloader.php';
include_once '../vendor/autoload.php';

use Database\SelectFromDb;
use Database\UpdateDb;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

$request = Request::createFromGlobals();
$session = new Session();
$old = input($request->request->get('oldpw'));
$pw1 = input($request->request->get('newpw1'));
$pw2 = input($request->request->get('newpw2'));

if(empty($old) || empty($pw1) || empty($pw2)) {
    echo '{"error":"Wypełnij wszystkie pola!"}';
    exit;
}

if($pw1 !== $pw2) {
    echo '{"error":"Podane hasła róznią się od siebie"}';
    exit;
}

try {
    $oldpw = (new SelectFromDb('accounts',array('password'),array('id'=>$session->get('LoginId'))))->result[0];
    if(password_verify($old,$oldpw['password'])) {
         new UpdateDb(array('password' => password_hash($pw1,PASSWORD_BCRYPT)),'accounts', $session->get('LoginId'));
        echo '{"error":false,"message":"Zmieniono hasło!"}';
    } else echo '{"error":"Błędne stare hasło!"}';
}catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}