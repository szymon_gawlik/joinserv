<?php
require_once '../Class/MyAutoloader.php';
include_once '../vendor/autoload.php';


use Database\SelectFromDb;

try {
    $server = (new SelectFromDb('servers',array('position','promo_amount'),array('promo'=>1)))->result;

    usort($server, function($a, $b) {
        return $a['position'] - $b['position'];
    });

    echo json_encode($server);
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}