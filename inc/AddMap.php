<?php
require_once '../Class/MyAutoloader.php';
require_once '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\InsertToDb;
use Database\SelectFromDb;
use Database\UpdateDb;
use Symfony\Component\HttpFoundation\Session\Session;

try {
    $request = Request::createFromGlobals();
    $action = $request->request->get('action');
    $file = $request->files->get('file');
    $fileExtension = array('jpg','jpeg','png');
    $extension = explode('.',$file->getClientOriginalName())[1];
    $session = new Session();

    if(!in_array($extension,$fileExtension)) {
        echo '{"error":"Błędny format obrazu! Dozwolone formaty: jpg,jpeg,png"}';
        exit;
    }

    switch($action) {
        case 'map':
            $mapName = input($request->request->get('name'));

            if(empty($mapName) || empty($file)) {
                echo '{"error":"Wypełnij wszystkie pola!"}';
                exit;
            }

            list($prefix,$name) = explode('_',$mapName);

            if(empty($prefix) || empty($name)) {
                echo '{"error":"Błędna nazwa mapy! Wprowadź format np. <strong>de_dust2</strong>"}';
                exit;
            }

            if($maps = (new SelectFromDb('map_image',array(),array('name'=>$mapName)))->result) {
                echo '{"error":"Obraz tej mapy już istnieje!"}';
                exit;
            }


            $imageSize = getimagesize($file->getPathName());

            if($imageSize[0] != '248' || $imageSize[1] != '186') {
                echo '{"error":"Błędny rozmiar obrazka! Wymagany: <strong>248px x 186px</strong>"}';
                exit;
            }
            $path = '../web/uploaded/maps/';
            $file->move($path,$mapName.'.'.$extension);

            new InsertToDb(array($mapName,0,$extension),'map_image');

            echo '{"error":false, "reload":true, "message":"Poprawnie dodano nowy obraz mapy! Musisz teraz poczekać aż administrator zaakceptuje mapę, po tym będzie wyświetlana w Szczegółach serwera!"}';

            break;

        case 'avatar':

            if(empty($file)) {
                echo '{"error":"Wypełnij wszystkie pola!"}';
                exit;
            }
            $path = '../web/uploaded/avatars/';
            $newFile = $session->get('LoginId').'.'.$extension;
            $file->move($path,$newFile);

            new UpdateDb(array('avatar'=>$newFile),'accounts',$session->get('LoginId'));

            echo '{"error":false,"message":"Poprawnie zmieniono avatar!"}';


            break;
    }
}catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}