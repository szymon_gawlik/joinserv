<?php
require_once '../../Class/MyAutoloader.php';
require_once '../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Database\SelectFromDb;
use Database\UpdateDb;
use homepay\Homepay;

try {
    $request = Request::createFromGlobals();
    $session = new Session();
    $check = input($request->request->get('check'));
    $id = $request->request->get('id');

    if(empty($check)) {
        echo '{"error":"Wpisz kod który otrzymałeś!"}';
        exit;
    }

    if(empty($id)) {
        echo '{"error":"Wybierz usługę!"}';
        exit;
    }

    $settings = (new SelectFromDb('sms_api',array('sms_id','number','code','amount'), array('id'=>$id)))->result[0];
    $mainSetting = new Homepay();

    $handle = fopen("http://homepay.pl/sms/check_code.php?usr_id=".$mainSetting->user_id."&acc_id=".$settings['sms_id']."&code=".$check, "r");
    $status = fgets($handle, 8);
    fclose($handle);

    $userWallet = (new SelectFromDb('accounts', array('wallet'), array('id' => $session->get('LoginId')), 1))->result[0];
    $newAmountUser = $settings['amount'] + $userWallet['wallet'];

    if($status == 0 || preg_match('/[^0-9A-Za-z]/', $check) )
    {
        echo '{"error":"Nieprawidlowy kod."}';
    }
    else if($status == 1) {
        new UpdateDb(array('wallet' => $newAmountUser), 'accounts', $session->get('LoginId'));
        echo '{"error":false,"message":"Poprawnie doładowano portfel kwotą: '.$settings['amount'].'zł"}';
    }
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}