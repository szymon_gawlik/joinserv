<?php
ini_set( 'display_errors', 'On' );
error_reporting( E_ALL );
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';
include 'function.php';
use Symfony\Component\HttpFoundation\RedirectResponse;
use Database\SelectFromDb;
use Database\InsertToDb;
use homepay\Homepay;
use Symfony\Component\HttpFoundation\Session\Session;

$homepay = new Homepay();
$session = new Session();
$config_homepay=["usr_id"=>$homepay->user_id,"acc_id"=>$homepay->acc_id,"acc_hash"=>$homepay->acc_hash];

if($_POST&&$_POST['init_transfer'])
{
    if(empty($_POST['amount'])) die("Musisz podac kwote");

    $date = (new DateTime('now', new DateTimeZone('Europe/Warsaw')))->format('d.m.Y H:i:s');
    new InsertToDb([$_POST['amount'], $session->get('LoginId'), $date], 'temp_order');
    $tempOrder = (new SelectFromDb('temp_order',[]))->result;
    $tempOrderId = $tempOrder[count($tempOrder)-1];

    $data=curlit("http://homepay.pl/API/init_transfer.php?usr_id=".$config_homepay['usr_id']."&acc_id=".$config_homepay['acc_id']."&acc_hash=".$config_homepay['acc_hash']."&amount=".$_POST['amount']."&name=".urlencode($_POST['name'])."&surname=".urlencode($_POST['surname'])."&control=".$tempOrderId['id']."&email=".urlencode($_POST['email']));


    $data=str_getcsv($data);

    if(count($data)!=2)
        echo $data; // wystapil blad
    else
    {
        $transfer_id=$data[0];
        $link=$data[1];

        // tu mozesz zapisac $transfer_id, np.
        // zapisz($transfer_id);

        // i wyswietlic klientowi link wplaty
        $redirect = RedirectResponse::create($link);
        $redirect->send();
        echo "<a href=".$link." target=\"_blank\">".$link."</a> - wplac przez Homepay<br/>\n";
    }
}

if($_POST&&$_POST['amount']) {
    $date = (new DateTime('now', new DateTimeZone('Europe/Warsaw')))->format('d.m.Y H:i:s');
    new InsertToDb([$_POST['amount'], $session->get('LoginId'), $date], 'temp_order');
    $tempOrder = (new SelectFromDb('temp_order',[]))->result;
    $tempOrderId = $tempOrder[count($tempOrder)-1];

    $key = ''; // KLUCZ PRYWATNY, HOMEPAY > PANEL PARTNERA > PAYSAFECARD > USTAWIENIA
    $data = [
        'uid' => $homepay->user_id, // IDENTYFIKATOR U�YTKOWNIKA HOMEPAY
        'public_key' => $homepay->public_key, // KLUCZ PUBLICZNY, HOMEPAY > PANEL PARTNERA > PAYSAFECARD > USTAWIENIA
        'amount' => ($_POST['amount'] * 100), // KWOTA W GROSZACH
        'label' => 'JoinServ.eu', // ETYKIETA P�ATNOSCI, np. ADRES.PL
        'control' => $tempOrderId['id'], // UNIKALNY IDENTYFIKATOR DLA P�ATNOCI NADAWANY PRZEZ PARTNERA
        'success_url' => urlencode('http://'.$_SERVER['SERVER_NAME'].'/?t=doladujPortfel'), // LINK NA KT�RY ZOSTANIE PRZEKIEROWANY KLIENT PRZY POMYSLNEJ P�ATNOSCI
        'failure_url' => urlencode('http://'.$_SERVER['SERVER_NAME'].'/?t=doladujPortfel&error=psc'), // LINK NA KT�RY ZOSTANIE PRZEKIEROWANY KLIENT W PRZYPADKU B��DU W P�ATNOSCI
        'notify_url' => urlencode('http://'.$_SERVER['SERVER_NAME'].'/inc/Homepay/return.php') // LINK NA KT�RY ZOSTANIE WYS�ANE POWIADOMIENIE API O DOKONANEJ P�ATNOSCI
    ];
    $data['crc'] = md5(join('', $data) . $key);

    echo '<form method="post" name="paysafecard" action="https://ssl.homepay.pl/paysafecard/">';
    foreach($data as $field => $value)
        echo '<input type="hidden" name="' . $field . '" value="' . $value . '">';


    echo '</form>
<p>Za chwile zostaniesz przekierowany do strony wpłaty.</p>
<p>Jeśli przekierowanie nie nastąpi w ciągu kilku sekund, kliknij <a href="#" onclick="document.paysafecard.submit(); return false;">tutaj</a>.</p>
<script>setTimeout(function() { document.paysafecard.submit(); }, 200);</script>';
}

