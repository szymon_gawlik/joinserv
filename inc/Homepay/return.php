<?php
require_once '../../Class/MyAutoloader.php';
include_once '../../vendor/autoload.php';

use Database\InsertToDb;
use Database\SelectFromDb;
use Database\Delete;
use Database\UpdateDb;

require_once("function.php");
if(!check_ip()) die(); // mozna wykomentowac ta linijke do testow, pozniej nalezy sprawdzac czy IP pochodzi od homepay

if(isset($_POST['csv'])) {
    $data = $_POST['csv'];

    $data = explode("\n", $data);
    foreach ($data as $trkey => $transfer) {
        if (!empty($transfer)) {
            $data[$trkey] = $transfer = str_getcsv($transfer);

            $acc_id = $transfer[0]; // numer uslugi (int)
            $id_partner = $transfer[1]; // id podane przez partnera w parametrze do inicjowania platnosci (int)
            $id_homepay = $transfer[2]; // id przelewu w homepay (int)
            $time = $transfer[3]; // czas (unix timestamp)
            $amount = $transfer[4]; // kwota (double)
            $guarantee = $transfer[5]; // gwarancja (int) "1" albo "0"
            $text = $transfer[6]; // opis przelewu, podany przez partnera, (string)
            $status = $transfer[7]; // zgodnie z dokumentacja Homepay, np. 1 dla oplaconego przelewu, 3 dla testowego (int)


            $tempOrder = (new SelectFromDb('temp_order', ['amount', 'user_id'], ['id' => $id_partner], 1))->result[0];
            $userWallet = (new SelectFromDb('accounts', ['wallet'], ['id' => $tempOrder['user_id']], 1))->result[0];
            $newAmountUser = $tempOrder['amount'] + $userWallet['wallet'];
            if ($tempOrder['amount'] == $amount) {
                new InsertToDb([$amount, $tempOrder['user_id'], $date], 'orders');
                new Delete('temp_order', $tempOrder['id']);
                new UpdateDb(['wallet' => $newAmountUser], 'accounts', $tempOrder['user_id']);
            }
        }
    }
} else if(isset($_POST['json'])) {
    $json = json_decode($_POST['json']);

    $result = [];
    $date = (new DateTime('now', new DateTimeZone('Europe/Warsaw')))->format('d.m.Y H:i:s');
    foreach($json as $payment)
    {
         $pay_id = $payment->psc_id; //- identyfikator płatności
         $user_id = $payment->psc_usr_id; //- identyfikator użytkownika Homepay
         $time = $payment->psc_time; //- znacznik czasu (unix timestamp)
         $status = $payment->psc_status; //- status płatności
         $amount = $payment->psc_amount; //- kwota płatności
         $priwizja = $payment->psc_provision; //- prowizja płatności (dla Homepay, prowizja użytkownika = 1 - $payment->provision)
         $email = $payment->psc_email; //- adres e-mail płacącego
         $etykieta = $payment->psc_merchant_label; //- etykieta płatności
         $id_partner =$payment->psc_merchant_data; //- unikalny identyfikator dla płatności nadawany przez partnera

        $tempOrder = (new SelectFromDb('temp_order', ['amount', 'user_id'], ['id' => $id_partner], 1))->result[0];
        $userWallet = (new SelectFromDb('accounts', ['wallet'], ['id' => $tempOrder['user_id']], 1))->result[0];
        $newAmountUser = $amount + $userWallet['wallet'];
        if ($tempOrder['amount'] == $amount) {
            new InsertToDb([$amount, $tempOrder['user_id'], $date], 'orders');
            new Delete('temp_order', $tempOrder['id']);
            new UpdateDb(['wallet' => $newAmountUser], 'accounts', $tempOrder['user_id']);
        }

        array_push($result, [
            'psc_id' => $payment->psc_id,
            'psc_return' => 1 // status płatności, inny niż 1 powoduje ponowienia odpytania przez API Homepay
        ]);
    }
}
