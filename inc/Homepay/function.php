<?php
if(!function_exists('curlit'))
{
    function curlit($link,$post=null)
    {
        $h=curl_init();
        curl_setopt($h,CURLOPT_URL,$link);
        curl_setopt($h,CURLOPT_HEADER,0);
        if(!is_null($post))
            curl_setopt($h,CURLOPT_POSTFIELDS,$post);

        curl_setopt($h,CURLOPT_RETURNTRANSFER,1);
        $r=curl_exec($h);
        curl_close($h);
        return $r;
    }
}
if (!function_exists('str_getcsv'))
{
    function str_getcsv($input,$delimiter=',',$enclosure='"')
    {
        $h=fopen("php://memory", "rw");
        fwrite($h,$input);
        fseek($h,0);

        $r=fgetcsv($h,4096,$delimiter,$enclosure);
        fclose($h);
        return $r;
    }
}
if(!function_exists('curlit1'))
{
    function curlit1($url)
    {
        if(!($h=fopen($url,'rb')))
            return false;

        $r=stream_get_contents($h);
        fclose($h);
        return $r;
    }
}
function check_ip()
{
    if(empty($_SERVER['REMOTE_ADDR']))

        return false;



    if(ini_get('allow_fopen_url') != 1)

        return gethostbyname('get.homepay.pl') == $_SERVER['REMOTE_ADDR'];



    $handle = fopen('http://get.homepay.pl/index.htm', 'r');

    $data = trim(stream_get_contents($handle));

    fclose($handle);

    return in_array($_SERVER['REMOTE_ADDR'], explode(',', $data));
}
?>