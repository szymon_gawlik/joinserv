<?php
require_once '../Class/MyAutoloader.php';
include_once '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\UpdateDb;
use Database\SelectFromDb;

$request = Request::createFromGlobals();

try {
    $content = input($request->request->get('content'));
    $id = $request->request->get('id');

    if(empty($id) || empty($content)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }

    if(new UpdateDb(array('content'=>$content),'reviews',$id)){
        echo '{"error":false, "message":"Poprawnie zmieniono opinie!"}';
    }
} catch (Exception $e){
    echo '{"error":"'.$e->getMessage().'"}';
}