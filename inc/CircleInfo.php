<?php
require_once '../Class/MyAutoloader.php';
require_once '../vendor/autoload.php';

use Database\SelectFromDb;

try {
    $servers = (new SelectFromDb('servers',[]))->result;
    $users = (new SelectFromDb('accounts',[]))->result;
    $networks = (new SelectFromDb('servers_network',[]))->result;

    $array = [
        'servers' => count($servers),
        'users' => count($users),
        'networks' => count($networks)
    ];

    echo json_encode($array);
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}

