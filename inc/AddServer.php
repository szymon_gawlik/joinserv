<?php
require_once '../Class/MyAutoloader.php';
include_once '../vendor/autoload.php';

use Database\InsertToDb;
use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

try {
    $request = Request::createFromGlobals();
    $session = new Session();
    $ip = input($request->request->get('ip'));
    $type = $request->request->get('type');

    if(empty($ip) || empty($type)) {
        echo '{"error":"Wypełnij pole!"}';
        exit;
    }

    list($adress, $port) = explode(':', $ip);

    if(isset($adress) && isset($port)) {
        if((new SelectFromDb('servers',array(),array('ip_adress'=>$ip,'ip_port'=>$port)))) {
            echo '{"error":"Serwer jest już w naszej bazie danych!"}';
            exit;
        }
        new InsertToDb(array('0',$adress,$port,$type,0,0,0,$session->get('LoginId'),0,0), 'servers');

        $newServer = (new SelectFromDb('servers', array('ip_adress', 'ip_port')))->result;

        $array = [
            'error' => false,
            'message' => 'Poprawnie dodano serwer!',
            'data' => $newServer[count($newServer)-1]
        ];
        echo json_encode($array);
    } else {
        echo '{"error":"Błędny adres ip!"}';
        exit;
    }

} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}