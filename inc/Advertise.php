<?php
require_once '../Class/MyAutoloader.php';
include_once '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Database\UpdateDb;
use Database\SelectFromDb;

try {
    $request = Request::createFromGlobals();

    $session = new Session();
    $server = input($request->request->get('server'));
    $amount = input($request->request->get('amount'));
    $expire = input($request->request->get('expire'));

    if(empty($server) || empty($amount) || empty($expire)) {
        echo '{"error":"Wypełnij wszystkie pola"}';
        exit;
    }

    list($ip,$port) = explode(':',$server);
    $wallet = (new SelectFromDb('accounts',array('wallet'),array('id'=>$session->get('LoginId')),1))->result[0];
    $setting = (new SelectFromDb('pricing',array('price','expire'),array('id'=>2)))->result[0];

    if($amount < $setting['price'] || $expire < $setting['expire']) {
        echo '{"error":"Minimalna kwota reklamy to: <strong>'.$setting['price'].'zł</strong>, a jej minimalny czas trwania: <strong>'.$setting['expire'].'dni</strong>"}';
        exit;
    }
    if($wallet['wallet'] < $amount*$expire) {
        echo '{"error":"Brak funduszy! Doładuj portfel!"}';
        exit;
    }
    $serverToPromo = (new SelectFromDb('servers',array('promo_amount'),array('ip_adress'=>$ip,'ip_port'=>$port)))->result[0];
    new UpdateDb(array('promo'=>1),'servers',$serverToPromo['id']);

    $promo = (new SelectFromDb('servers',array('promo_amount','promo_expire','ip_adress','ip_port'),array('promo'=>1)))->result;

    for($i=0;$i<count($promo);$i++) {
        if($promo[$i]['id'] == $serverToPromo['id']) {
            $promo[$i]['promo_amount'] = intval($amount);
        }
    }

    usort($promo, function($a, $b) {
        return $a['promo_amount'] - $b['promo_amount'];
    });

    $promo = array_reverse($promo);

    $date = (new DateTime("+$expire days", new DateTimeZone('Europe/Warsaw')))->format('d.m.Y H:i:s');
    for($i=0;$i<count($promo);$i++){
        if(empty($promo[$i]['promo_expire']) || $promo[$i]['promo_expire'] == 0) {
            new UpdateDb(array('position'=>$i+1,'promo_expire'=>$date,'promo_amount'=>$amount),'servers',$promo[$i]['id']);
            continue;
        }
        new UpdateDb(array('position'=>$i+1),'servers',$promo[$i]['id']);
    }

    new UpdateDb(array('wallet'=>($wallet['wallet']-$amount*$expire)),'accounts',$session->get('LoginId'));

    echo '{"error":false, "message":"Poprawnie dodano!"}';
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}