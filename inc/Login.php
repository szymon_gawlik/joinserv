<?php
require_once '../Class/MyAutoloader.php';
include_once '../vendor/autoload.php';

use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;

try {
    $request = Request::createFromGlobals();
    $session = new Session();

    $login = input($request->request->get('login'));
    $pw = input($request->request->get('pw'));
    if($check = (new SelectFromDb('accounts',array('password','admin'),array('login' => $login),1))->result) {
        if(password_verify($pw,$check[0]['password'])) {
            $session->set('LoginId', $check[0]['id']);
            $session->set('Permission', $check[0]['admin']);
            echo '{"error":false,"message":"Zalogowano!"}';
        } else echo '{"error":"Błędne dane! Spróbuj ponownie!"}';
    } else echo '{"error":"Błędne dane! Spróbuj ponownie!"}';
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}