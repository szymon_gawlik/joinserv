<?php
require_once '../Class/MyAutoloader.php';
include_once '../vendor/autoload.php';
include_once '../inc/recaptcha.php';
use Database\SelectFromDb;
use Database\InsertToDb;
use Symfony\Component\HttpFoundation\Request;

try {
    $request = Request::createFromGlobals();
    $login = input($request->request->get('login'));


    if (empty($login)) {
        echo '{"error":"Pole <strong>Login</strong> jest obowiązkowe!"}';
        exit;
    }

    if ($check = (new SelectFromDb('accounts', array('login'), array('login' => $login)))->result) {
        echo '{"error":"Login <strong>' . $login . '</strong> jest zajęty!"}';
        exit;
    }

    $pw = input($request->request->get('pw'));
    $pw2 = input($request->request->get('pw2'));
    $mail = input($request->request->get('mail'));
    $date = (new Datetime('NOW', new DateTimeZone('Europe/Warsaw')))->format('d.m.Y H:i:s');
    $recaptcha = $request->request->get('g-recaptcha-response');

    $secret = '6LcTxBATAAAAAGZ1lHXpLToiFMvoaiFhn2KGwHKh';
    $response = null;
    $reCaptcha = new ReCaptcha($secret);

    if (empty($pw) || empty($pw2) || empty($mail)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }

    if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
        echo '{"error":"Błędny adres email!"}';
        exit;
    }

    if ($pw !== $pw2) {
        echo '{"error":"Podane hasła różnią się od siebie!"}';
        exit;
    }

    if ($email = (new SelectFromDb('accounts', array(), array('email' => $mail)))->result) {
        echo '{"error":"Podany adres email istnieje w naszej bazie!"}';
        exit;
    }

    if ($recaptcha) {
        $response = $reCaptcha->verifyResponse(
            $_SERVER['REMOTE_ADDR'],
            $recaptcha
        );
    } else {
        echo '{"error":"Nie podałeś kodu bezpieczeństwa! Spróbuj ponownie!"}';
        exit;
    }

    if ($response != null && $response->success) {
        if (new InsertToDb(array($login, password_hash($pw, PASSWORD_BCRYPT), $mail, $date, 0, 0, 'avatar.png'), 'accounts')) {
            echo '{"error":false,"message":"Poprawnie zarejestrowano! Teraz możesz się zalogować!","reload":"index.php"}';
        }
    } else {
         echo '{"error":"Błędna weryfikacja!"}';
    }
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}