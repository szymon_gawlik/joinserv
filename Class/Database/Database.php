<?php
namespace Database;
class Database
{
    protected $conn;

    public function __construct()
    {
        $this->connection();
    }

    private function connection()
    {

        $connInfo = [
            'host' => 'localhost',
            'login' => 'root',
            'password' => '',
            'dbName' => 'serwery'
        ];
        $connection = new \mysqli($connInfo['host'],$connInfo['login'], $connInfo['password'], $connInfo['dbName']);

        if($connection->connect_error) {
            throw new \Exception('Blad polaczenia z baza danych! ' .$connection->connect_error, E_USER_ERROR);
        } else {
            $connection->set_charset('utf8');
            $this->conn = $connection;
        }
    }

}