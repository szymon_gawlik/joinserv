<?php
namespace homepay;

use Database\SelectFromDb;

class Homepay extends SelectFromDb
{

    public $user_id;
    public $acc_id;
    public $acc_hash;
    public $public_key;
    public $private_key;

    public function __construct()
    {
        $settings = (new SelectFromDb('homepay_settings',['id_user','acc_id','acc_hash','public_key','private_key'],null,1))->result[0];

        $this->user_id = $settings['id_user'];
        $this->acc_id = $settings['acc_id'];
        $this->acc_hash = $settings['acc_hash'];
        $this->public_key = $settings['public_key'];
        $this->private_key = $settings['private_key'];
    }
}