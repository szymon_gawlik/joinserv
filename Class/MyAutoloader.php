<?php
class myAutoloader
{
    public static function autoload($class)
    {
        $class = str_replace('\\', '/', $class);
        include_once "$class.php";

    }
}

spl_autoload_register(array('myAutoloader', 'autoload'));


function input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}